(function() {
  function getIframeWindow(iframe_object) {
    var doc;
  
    if (iframe_object.contentWindow) {
      return iframe_object.contentWindow;
    }

    if (iframe_object.defaultView) {
      return iframe_object.defaultView;
    }
  
    if (iframe_object.window) {
      return iframe_object.window;
    } 
  
    if (!doc && iframe_object.contentDocument) {
      doc = iframe_object.contentDocument;
    } 
  
    if (!doc && iframe_object.document) {
      doc = iframe_object.document;
    }
  
    if (doc && doc.defaultView) {
     return doc.defaultView;
    }
  
    if (doc && doc.parentWindow) {
      return doc.parentWindow;
    }
  
    return undefined;
  }
	function setEditor(el) {
		const editor = ace.edit(el);
		editor.setTheme('ace/theme/eclipse');
		editor.setOptions({
			enableEmmet: true,
			enableBasicAutocompletion: true,
			enableLiveAutocompletion: true,
			enableSnippets: true,
			spellcheck: true,
      tabSize: 2,
      showPrintMargin: false
		});
		editor.session.setMode('ace/mode/html');
    let iframe = document.getElementById(el.dataset.target);
    let render;
    let tmo;
		function showHTML() {
			iframe = document.getElementById(el.dataset.target).contentDocument;
      const main = iframe.querySelector('.main');
      if (tmo) {
        clearTimeout(tmo);
      }
			if (!main) {
				tmo = setTimeout(() => {
					showHTML();
				}, 500);
				return;
      }
      if (!render) {
        render = orly.bind(main);
      }
      render`<span></span>`;
      render`${{ html: editor.getValue() }}`;
      [...iframe.querySelectorAll('.modal-backdrop')].forEach(el =>
				el.remove()
      );
			setTimeout(() => {
        getIframeWindow(iframe).loaded && getIframeWindow(iframe).loaded();
				//takes a sec to render the el in ff
				//main.querySelector('orly-dialog').open();
			}, 100);
		}
		editor.on('input', showHTML);
		iframe.onload = function() {
			showHTML();
		};
		const btn = el.parentNode.parentNode.querySelector('.replay');
		btn.addEventListener('click', showHTML);
    btn.click();
    el.classList.remove("invisible");
	}

	document.addEventListener('click', function(e) {
		const tag = e.target;
		if (tag.classList.contains('resize')) {
      const el = tag.parentNode.parentNode.querySelector('iframe');
      if (el.classList.contains("expanded")) {
        el.classList.remove("expanded");
        el.classList.add("contracted");
      } else if (el.classList.contains("contracted")) {
        el.classList.remove("contracted");
      } else {
        el.classList.add("expanded");
      }
		}
  });
  
  [...document.querySelectorAll('.dialog-editor')].forEach(setEditor);
	document.querySelector('.main').classList.remove('d-none');
})();
